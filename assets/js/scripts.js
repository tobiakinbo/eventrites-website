
$( document ).ready(function() {
	  /* eliminate affix jump */
    $('.navbar-wrapper').height($(".navbar").height());

    new WOW().init();

    $('.responsive-calendar').responsiveCalendar();
    

    $('#myTab a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });

    $(function(){
    $('a[title]').tooltip();
    });

    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="optionimage"){
            $(".optionradio").not(".optionimage").hide();
            $(".optionimage").show();
        }
        if($(this).attr("value")=="optionvideo"){
            $(".optionradio").not(".optionvideo").hide();
            $(".optionvideo").show();
        }
    });




      $('input.typeahead').typeahead({
    name: 'accounts',
    local: ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen']
  });


          $("button").click(function(){
              $("p").slideToggle();
          });
  


+ function($) {
    'use strict';

    // UPLOAD CLASS DEFINITION
    // ======================

    var dropZone = document.getElementById('drop-zone');
    var uploadForm = document.getElementById('js-upload-form');

    var startUpload = function(files) {
        console.log(files)
    }

    uploadForm.addEventListener('submit', function(e) {
        var uploadFiles = document.getElementById('js-upload-files').files;
        e.preventDefault()

        startUpload(uploadFiles)
    })

    dropZone.ondrop = function(e) {
        e.preventDefault();
        this.className = 'upload-drop-zone';

        startUpload(e.dataTransfer.files)
    }

    dropZone.ondragover = function() {
        this.className = 'upload-drop-zone drop';
        return false;
    }

    dropZone.ondragleave = function() {
        this.className = 'upload-drop-zone';
        return false;
    }

}(jQuery);



    
});

//scroll to element functionality
    $(document).on('click','[role="scroll-to-element"]',function(event){

        var scrollToElem = $(this).attr('data-element');
        var elem = $('#'+scrollToElem);
            
        $('html,body').animate({
            scrollTop: elem.position().top
        },1000,function(){
            //do stuff like activate scrollr or some shit                 
        });

        event.preventDefault();
    });  


$(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
        $('#back-to-top:hidden').stop(true, true).fadeIn();
    } else {
        $('#back-to-top').stop(true, true).fadeOut();
    }
});


$(window).load(function() {

 /* activate jquery isotope */
  var $container = $('#posts').isotope({
    itemSelector : '.item',
    isFitWidth: true

  });

 $container.isotope({
  getSortData : {
       date : function ( $container ) {
      return $container.find('.date').attr('data-date');
     }
    },
    sortBy : 'date',
    sortAscending : true

  });

  $container.isotope({ filter: '*' });


    // filter items on button click
  $('#filters').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $container.isotope({ filter: filterValue });
    
  });

    // bind filter on select change
  $('#filters').on( 'change', function() {
    // get filter value from option value
    var filterValue = this.value;
    // use filterFn if matches value
    filterValue = filterValue;
    $container.isotope({ filter: filterValue });
  });
  

  var images = ['background.jpg', '1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg'];
  $('.jumbotron').css({'background-image': 'url(assets/img/' + images[Math.floor(Math.random() * images.length)] + ')'});
       

  var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;
 
    // an array that will be populated with substring matches
    matches = [];
 
    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');
 
    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });
 
    cb(matches);
  };
};
 
var username = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

var location = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
  'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
  'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
  'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
  'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
  'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
  'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];
 
$('#location .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 3
},


{
  name: 'location',
  source: substringMatcher(location)
});


$('#username .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 3
},


{
  name: 'username',
  source: substringMatcher(username)
});


});



